
<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/*
 Template Name: News Archive Template
 */
get_header(); 

$queried_object = get_queried_object();

$landing_page_object = get_field('landing_page_relationship', $queried_object);
// Custom header as background image
$header_image = get_field('custom_header_image', $landing_page_object->ID);
if (isset($landing_page_object->ID)){
	echo '<style type="text/css">'.get_post_meta($landing_page_object->ID, '_custom_css', true).'</style>';
	$landing_class = " landing-hero";
}
if( !empty($header_image) ){ ?>
<style>
.custom-header-img {
	background-image: url('<?php echo $header_image['sizes'][ 'custom-header' ]; ?>');
}
</style>
<?php } ?>
 
	

<div class="container">
<div id="content" >

	<div id="main-content" class="news-archive">
	
	
			
		<div id="content-wrap" class="col-1" style="clear:both;">
			<div class="col-md-8 col-sm-8 post-content">
				<div class="headering-top">
			 
			<div class="col-md-12 col-sm-6"><h1 style="padding-bottom:0;"><?php the_title(); ?></h1> </div>

  
		</div>
							
				<?php 
						$category_name = get_field('archive_category_name');
						$number = get_field('archive_no_of_post');
						 $current_month = date('m');
						
						 $paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$args = array(
							'post_type' => 'post',
							'orderby' => 'date',
							'order' => 'desc',
						'paged' => $paged, 
							//'monthnum' => $current_month, 
							'category_name' => $category_name, 
							'ignore_sticky_posts' => 1
							
						);
						// The Query
						$the_query = new WP_Query( $args );

						// The Loop
						if ( $the_query->have_posts() ) {
							
							while ( $the_query->have_posts() ) {
								$the_query->the_post(); ?>
				
							<div class="col-sm-12 news-box-archive">
							<?php if ( has_post_thumbnail()) :
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        $slider_img = $thumb_image_url[0];
      endif; ?>
		 <div class="col-md-4 col-sm-4"><div class="feature-img"><img src="<?php echo $slider_img;?>" alt="" /> </div></div>
	<div class="col-md-8 col-sm-8">	<a href="<?php the_permalink(); ?>">
			
			<h4><?php echo ShortenText( 40, get_the_title(), false ); 
			//echo softTrim(the_title(), 6);?></h4>
		
		</a>
		<div class="news-content"><?php echo ShortenText( 200, get_the_content(), false );?> </div>
		</div>
		</div>			<?php	}

			
					if( function_exists('wp_pagenavi'))  {  
					
echo "<div class='pagination'>";
						wp_pagenavi(array('query' => $the_query)); 
	echo "</div>";
					
				}
			
					
							
						} else {
							// no posts found
						}
						/* Restore original Post Data */
						wp_reset_postdata();
					 ?>
			
				</div>
				<?php get_sidebar(); ?>
			</div><!-- end col-1 -->
<div class="subsribe-box">
	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="main">STAY UPDATED with bulleen bulletins</div>
			<span>All the latest club news straight to your inbox! </span>
	</div>

	<div><?php echo do_shortcode('[gravityform id=1 ajax=true title=false description=false]');?></div>
</div>
			<?php // get_sidebar('archives'); ?>
		</div><!-- end row -->
	</div><!-- end of #content -->

</div>
<div class="container">

<?php  get_template_part( 'includes/sponsors' ); ?>

</div>

<?php get_footer(); ?>