<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
  exit;
}

get_header(); ?>

<?php

$landing_page_object = get_field('landing_page_relationship');

// Custom header as background image
$header_image = get_field('custom_header_image', $landing_page_object->ID);
if( !empty($header_image) ){ ?>
<style>
<?php
  if (isset($landing_page_object->ID)){
    echo '<style type="text/css">'.get_post_meta($landing_page_object->ID, '_custom_css', true).'</style>';
    $landing_class = " landing-hero";
  }
?>
</style>
<?php } ?>

<?php
  if (isset($landing_page_object->ID)){
    ?>
    <script>
    jQuery(document).ready(function($) {
      $('body').addClass('is-landing');
    });
    </script>
<?php } ?>
<?php

if($landing_page_object){

$menu_id = get_field('sub_menu', $landing_page_object->ID);


if($menu_id){
?>
<div class="landing-nav">
  <div class="container">
    <?php wp_nav_menu( array(
                 'container'       => 'div',
                 'container_class' => 'landing-menu',
                 'fallback_cb'     => 'responsive_fallback_menu',
                 'menu'  => $menu_id
               )
    );
    ?>    
  </div>
</div><!-- /.landing-nav -->
<?php } } 
?>
  <div class="container">
  <div id="content-wrap" >

  <!--  <div class="col-md-12">
      <?php  //get_template_part( 'loop-header' ); ?>
    </div> -->
  
    
  <div id="content" class="col-1" style="clear:both;">
      
    <div class="col-md-12 col-sm-12 post-content">
      <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
            
          <h1 class="news-title"><?php the_title(); ?></h1>
           
      <?php 
           
          
        
           the_content(); ?>
          <?php endwhile; ?>
          <?php else : ?>
            <p>No Posts.</p>
      <?php endif; ?>
      
      <?php// get_template_part( 'includes/next-prev' ); ?>
      <?php //get_template_part( 'includes/related-articles' ) ?>

    </div>

      <?php// get_sidebar(); ?>

</div>

<div class="subsribe-box">
  <div class="col-md-4">
      <div class="main">STAY UPDATED with bulleen bulletins</div>
      <span>All the latest club news straight to your inbox! </span>
  </div>

  <div class="col-md-8"><?php echo do_shortcode('[gravityform id=1 ajax=true title=false description=false]');?></div>
</div>
  </div>

</div>

<div class="container">

<?php  get_template_part( 'includes/sponsors' ); ?>

</div>
<?php get_footer(); ?>
