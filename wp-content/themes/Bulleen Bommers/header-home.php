	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		
	
	</head>

<body <?php body_class(); ?>>

dfdsfsfsdf
<?php /*?>

<div class="top-header"> 
	

	<div class="container">
	<div class="col-md-6 col-sm-7 col-xs-10 header-logo">
		<div class="top-logo"> <a href="<?php echo home_url( ); ?>" > <img src="<?php echo get_field('top_logo','option');?>"></a></div>
		<div class="logo-text">
			<div class="bold-text"><?php echo get_field('logo_text_top','option'); ?> </div>
			<div class="small-text"> <?php echo get_field('logo_text_bottom','option'); ?></div>
		</div>
	</div>

	<div class="col-md-3 col-sm-5">
		<div class="header-spons">
			<ul>
			<?php $hspns = get_field('header_right_logos','option'); 

			foreach($hspns as $logos) { ?>
				<li> <a href="<?php echo $logos['link']; ?>"> 
					<img src="<?php echo $logos['image']; ?>">
				</a></li>
			<?php }
			?>
		</ul>
		</div>
	</div>
	<div class="col-sm-3 social-media-icon">
		<div class="social-search">
			<div class="social-search-list"> Follow WRFL ON </div> 
			<div class="social-icon"> 
				<?php 
				$facebook = get_field('facebook', 'option');
				$twitter = get_field('twitter', 'option');
				$instagram = get_field('instagram', 'option');
				$youtube = get_field('youtube', 'option');


				?>
				<?php if($facebook != "") { ?>
					<a href="https://www.facebook.com/<?php echo $facebook;?>" target="_blank"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a> 
				<?php } ?>

				<?php if($twitter != "") { ?>
					<a href="https://twitter.com/<?php echo $twitter;?>" target="_blank"> <i class="fa fa-twitter-square" aria-hidden="true"></i> </a> 
				<?php } ?>

				<?php if($instagram != "") { ?>
					<a href="https://www.instagram.com/<?php echo $instagram;?>" target="_blank"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> 
				<?php } ?>
	</div>
			
	 
			</div>
		
	 </div>
</div>
	
</div>
<!-- Nav Wrap START -->
<div id="nav-container" >


	<div class="container">

			<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
			
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);

			?>
		<div class="search-box">

				<div id="sb-search" class="sb-search">
					    <form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					        <input class="sb-search-input" placeholder="Type search team here..." type="search" value="" name="s" id="search">
					        <i class="fa fa-search header_search"></i>
					        <input class="sb-search-submit" type="submit"  value="<?php echo wp_specialchars( $s, 1 ); ?>">
					        <span class="sb-icon-search"></span>
					    </form>
					</div>

			</div>
	</div>
</div>
<?php */?>
<!-- Nav Wrap END -->

