<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
 
 

<div class="container"><div class="footer-outer">

<div class="footer">
 
		<div class="quick-links"> 
		<h4>MORE B-BALL </h4>
		 <?php  if ( has_nav_menu( 'footer-menu' ) ) {
 wp_nav_menu( array(
		'container'       => 'div',
		'container_class' => 'top-nav',
		'fallback_cb'     => 'responsive_fallback_menu',
		'theme_location'  => 'footer-menu',
		'menu'     => 'Footer Menu'
	)
);
}?>
		</div>
		

		
		<div class="footer-contact"> 
			<h4> Contact Us</h4>
			<div class="contact-outer">
		
			<div class="details"><?php echo get_field('contact_details','option'); ?></div>
			<div class="address"><?php echo get_field('contact_address','option');?> </div>
		</div>
		</div>
	

	<div class="footer-logo"> 
		<a href="<?php echo home_url( ); ?>" ><img src="<?php echo get_field('footer_logo','option'); ?>"></a>
		<img src="<?php echo get_field('other_logo','option'); ?>">
	</div>
	 
	</div>
 
</div>
</div>

</div>

<div class="last-footer">
	<p> Copyright © 2019 Bulleen Boomers. Site by <a href="https://www.mmr.com.au/" target="_blank">MMR</a>  </p>
	</div>
<?php wp_footer(); ?>
<!-- <script>
	new UISearch( document.getElementById( 'sb-search' ) );
</script> -->
</body>
</html>