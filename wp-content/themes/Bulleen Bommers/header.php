	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		<script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js" integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


	<style type="text/css">
	body {
		    background: url('<?php echo get_field('background_image','option');?>') no-repeat;
		    background-position: center top;
		    background-attachment: fixed;
		    background-size: cover;
		}
	</style>
	
	</head>


<body <?php body_class(); ?>>

<?php $backheader =  get_field('header_background','option'); ?>
 
<div class="white-bg">

	<div class="container">

<div class="top-header" > 
	

	<div class="header-logo">
		<div class="top-logo"> <a href="<?php echo home_url( ); ?>" > <img src="<?php echo get_field('top_logo','option');?>"></a>
		</div>
		 
	</div>

	<div class="text">
	    <?php echo get_field('header_text','option');?>
	</div>

	<div class="register-buttons">
		 <a href="<?php echo get_field('register_link','option');?>"> Registrations</a> 


		<div class="simple-button1"> 

<?php /*wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
			
					'theme_location'  => 'sub-header-menu',
					'menu'  => 'Top Right Menu'
				)
			);*/

			?>
		</div>
	</div>

</div>

<!-- Nav Wrap START -->
<div id="nav-container" >

			<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
			
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);

			?>
 


			<div class="search-box">
				<form method="get" class="clearfix" id="searchform" action="<?php echo home_url( '/' ); ?>">
				<div>
					<input type="text" size="28" value="<?php echo wp_specialchars( $s, 1 ); ?>" name="s" id="s" placeholder="Search" />
					 <input type="submit" id="searchsubmit" value="s" />
				</div>
			</form>
			</div>
 
</div>
<!-- Nav Wrap END -->

</div>	
 

