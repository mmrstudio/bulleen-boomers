<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); 

?>
<div class="container">
<div id="content"> 

	<div id="main-content" class=" news-archive">
		<div id="content-wrap" class="col-1" style="clear:both;">
			<div class="col-md-12 post-content">
			<?php  //get_template_part( 'loop-header' ); ?>
			<?php if( is_category() || is_tag() || is_author() || is_date() ) { ?>
			<div class="headering-top-archive">
			<div class="col-md-5">	<h1 class="news-title">
					<?php
					if( is_day() ) :
						printf( __( 'Daily Archives: %s', 'responsive' ), '<span>' . get_the_date() . '</span>' );
					elseif( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'responsive' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
					elseif( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'responsive' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
					elseif( is_category()) :
						printf(__('%s'), single_cat_title('', false));
					else :
						_e( 'News Archives', 'responsive' );
					endif;
					?>
				</h1> </div>

			</div>
				<?php } ?>
				<?php if (isset($landing_page_object->ID)){ ?>
				<?php if( !empty($header_image) ){ ?>
						<h1 class="page-title-landing"><?php printf(__('%s'), single_cat_title('', false));?></h1>
				<?php } } ?>

			
		 
			<div class="col-md-8 col-sm-8 post-content">
		 
							
				<?php 
						$category_name = get_field('archive_category_name');
						$number = get_field('archive_no_of_post');
						 $current_month = date('m');
						
						 $paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$args = array(
							'post_type' => 'post',
							'orderby' => 'date',
							'order' => 'desc',
						'paged' => $paged, 
							//'monthnum' => $current_month, 
							'category_name' => $category_name, 
							'ignore_sticky_posts' => 1
							
						);
						// The Query
						$the_query = new WP_Query( $args );

						// The Loop
						if ( $the_query->have_posts() ) {
							
							while ( $the_query->have_posts() ) {
								$the_query->the_post(); ?>
				
							<div class="col-sm-12 news-box-archive">
							<?php if ( has_post_thumbnail()) :
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        $slider_img = $thumb_image_url[0];
      endif; ?>
		 <div class="col-md-4 col-sm-4"><div class="feature-img"><img src="<?php echo $slider_img;?>" alt="" /> </div></div>
	<div class="col-md-8 col-sm-8">	<a href="<?php the_permalink(); ?>">
			
			<h4><?php echo ShortenText( 40, get_the_title(), false ); 
			//echo softTrim(the_title(), 6);?></h4>
		
		</a>
		<div class="news-content"><?php echo ShortenText( 200, get_the_content(), false );?> </div>
		</div>
		</div>			<?php	}

			
					}
						/* Restore original Post Data */
						wp_reset_postdata();
					 ?>
			
				</div>
				<?php get_sidebar(); ?>
		 
		</div><!-- end row -->

		</div>
		<div class="subsribe-box">
	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="main">STAY UPDATED with bulleen bulletins</div>
			<span>All the latest club news straight to your inbox! </span>
	</div>

	<div><?php echo do_shortcode('[gravityform id=1 ajax=true title=false description=false]');?></div>
</div>
	</div><!-- end of #content -->
	</div></div>
<?php get_footer(); ?>
