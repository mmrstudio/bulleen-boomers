<div class="social-buttons">
	<div class="text"> Follow us</div>
	<div class="social-icons">
		<div class="icons"> <a href="https://www.facebook.com/BulleenTemplestoweBasketballClub/" target="_blank">  
			<i class="fab fa-facebook-f icon-5x"></i></a> 
		</div>

		<div class="icons"> <a href="https://www.instagram.com/bulleenboomersinsta/" target="_blank"> 
			<i class="fab fa-instagram icon-5x"></i></a> 
		</div>
	</div>
</div>
<div class="social-feed">
	<div class="col-sm-6 col-xs-12"><?php echo do_shortcode('[WD_FB id="1"]');?></div>
	<div class="col-sm-6 col-xs-12"><?php echo wdi_feed(array('id'=>'1')); ?></div>
</div>