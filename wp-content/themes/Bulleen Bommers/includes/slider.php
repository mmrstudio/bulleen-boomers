
<?php 
  $args = array(
    'post_type' => 'post',
    'category_name' => 'slider',
    'posts_per_page' => '6'
  );
  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) {
?>
<div class="slick-slider-container">
  
  <div class="mv-slider ">
  <?php 
    global $seen_posts;
    $seen_posts = array();
    while ( $the_query->have_posts() ) {
    $the_query->the_post();
    $seen_posts[] = $post->ID;
      if ( has_post_thumbnail()) {
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        $slider_img = $thumb_image_url[0];
       } else { 
        if (getFeaturedVideoPreview($post->ID) !="") {
          $yt_url = get_post_meta($post->ID, 'featuredVideoURL', true);
          $slider_img_part = substr( $yt_url, strrpos( $yt_url, '=' )+1 );
          $slider_img = 'http://img.youtube.com/vi/'.$slider_img_part.'/hqdefault.jpg';
        }else{
          $slider_img = get_stylesheet_directory_uri().'/core/images/placeholder.jpg';
        }
        ?>
        <?php } ?>
        <li class="mv-slide" >
          <img src="<?php echo $slider_img; ?>">
         
           <!--    <div class="caption">
                  <div class="small-text"> <?php echo get_field('slider_caption'); ?></div>
                  <div class="slider-title"><a href="<?php the_permalink() ?>"><span><?php echo ShortenText( 30, get_the_title(), false ); ?></span></a> 
                  </div>
                
              </div> -->
        </li>
      <?php } ?>

           

  </div>
  
 

</div>
<?php } ?>



<script>
  jQuery(document).ready(function($) {
    $('.mv-slider').slick({

    dots: true,
    infinite: true,
    arrows: false,
    autoplay:true,
    speed: 1000,
    slidesToShow: 1,
    fade: true,
    adaptiveHeight: true,
   // adaptiveHeight: true,
  cssEase: 'linear'

    
    });


    


  });
</script>