<div class=" sponsor">

<p class="center">With thanks to our sponsors </p>

<div class="border"> </div>

<div class="sub-title"> <?php echo get_field('sponsors_1_title','option'); ?></div>
<?php $sponsors = get_field('sponsors','option');
      
        if($sponsors)
        {
          echo '<ul class="sponsors clearfix mob-slider">';

          foreach($sponsors as $logos)
          {
            echo '<li><a href="'.$logos['link'].'"><img src="'.$logos['image'].'" /> </a>';
          }

          echo '</ul>';
        } 
        
        ?>
       

        <script>
          jQuery(document).ready(function($) {


              $('.mob-slider').slick({
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                                                                                                                                              
                 responsive: [
                  {
                    breakpoint: 769,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                      autoplay:true,
                      arrows:false,
                      dots:true
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                       autoplay:true,
                      arrows:false,
                      dots:true
                    }
                  }
                ]
                
              });
          });
          </script>
</div>