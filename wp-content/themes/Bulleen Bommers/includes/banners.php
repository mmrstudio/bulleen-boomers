<div class="banner-outer"> 
<ul class="banners">
 <?php 
 	$banners = get_field('banner_images','option');

 	foreach($banners as $banner) { ?>
 		<li> <img src="<?php echo $banner['image']; ?>"></li>
 	<?php }
 ?>
 </ul>
</div>
<script>
  jQuery(document).ready(function($) {


$('.banners').slick({
  dots: true,
  arrows: false,
  infinite: true,
  speed: 300,
  autoplay:true,
  slidesToShow: 1
  
});
	

  });
</script>