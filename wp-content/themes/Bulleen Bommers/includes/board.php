<div class="container">
	<div class="col-sm-12 staff-upper">
		<div class="col-md-4 col-sm-3 col-xs-12">    <h1 class="solution-title">Key People</h1> </div>
		<div class="col-md-8 col-sm-9 col-xs-12"><p> The Upper Yarra Mineral Springs Company brings together a team of widely experienced people with the expertise to deliver both a unique product and the strategic marketing to support its introduction into any market. </p>
		</div>
	</div>
	<div class="col-sm-12"> 
		 
				<?php $staff = get_field('board','option');
				foreach($staff as $wrflstaff) { ?>
					<div class="col-md-4 col-sm-6 col-xs-12 staff-outer"> 
						<div class="staff-image"><img src="<?php echo $wrflstaff['image']; ?>"> </div>
						<div class="staff-name"> <h2><?php echo $wrflstaff['name']; ?></h2></div>
						<div class="staff-pos"> <h3><?php echo $wrflstaff['position']; ?></h3></div>
						<div class="staff-company"> (<?php echo $wrflstaff['company']; ?>)</div>
					 	<div class="staff-desc"> <?php echo $wrflstaff['description']; ?></div>
					</div>
				<?php }
				?>
	</div>
</div>