<div class=" news-panel">

  <?php
  $args = array(
							'post_type' => 'post',
							'orderby' => 'date',
							'order' => 'desc',
						
							//'monthnum' => $current_month, 
							'category_name' => 'latest-news', 
							'posts_per_page' => 1,
							'ignore_sticky_posts' => 1
							
						);
  	// The Query
						$the_query = new WP_Query( $args );

						// The Loop
					if ( $the_query->have_posts() ) {
							
							while ( $the_query->have_posts() ) {
								$the_query->the_post(); ?>
				
							<div class="news-box">
							<?php if ( has_post_thumbnail()) :
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        $slider_img = $thumb_image_url[0];
      endif; ?>
		 <div class="feature-img"><img src="<?php echo $slider_img;?>" alt="" />

		 <div class="news-cat">
		 	Latest News
		 </div>

		  <div class="news-cat-mobile">
		 	 News
		 </div>

		 <div class="news-title">
		 	<a href="<?php the_permalink(); ?>"><h4><?php echo ShortenText( 40, get_the_title(), false ); 
			//echo softTrim(the_title(), 6);?></h4></a>
			 <div class="news-more">
		 	<a href="<?php the_permalink(); ?>"> Read more > </a>
		 </div>
		</div>



 </div>

		
		</div>			<?php	}

			
				
						}
						/* Restore original Post Data */
						//wp_reset_postdata();
				 
     ?>
</div>

<div class="subsribe-box col-xs-12">
	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="main">STAY UPDATED with bulleen bulletins</div>
			<span>All the latest club news straight to your inbox! </span>
	</div>

	<div><?php echo do_shortcode('[gravityform id=1 ajax=true title=false description=false]');?></div>
</div>